﻿using System;
using System.Runtime.Serialization;

namespace OnlineStore
{
    [Serializable]
    public class NotFindException : Exception
    {
        public NotFindException()
        {
        }

        public NotFindException(string message)
            : base(message)
        {
        }

        public NotFindException(string message, Exception inner)
            : base(message, inner)
        {
        }
        protected NotFindException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }
    }
}
