﻿using System;
using System.Runtime.Serialization;

namespace OnlineStore.Products
{
    [Serializable]

    public class NullReferenceProductException : Exception
    {
        public NullReferenceProductException()
        {
        }

        public NullReferenceProductException(string message)
            : base(message)
        {
        }

        public NullReferenceProductException(string message, Exception inner)
            : base(message, inner)
        {
        }
        protected NullReferenceProductException(SerializationInfo info, StreamingContext context)
    : base(info, context)
        {

        }

    }
}
