﻿using System;

namespace OnlineStore.Products
{
    public class Product : IProduct
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public double Price { get; set; }
        public Product(string name, string descripton, string category, double price)
        {
            if (name == null && descripton == null && category == null && price == 0)
                throw new ArgumentNullException(name, "Arguments can not be null or equal to 0");
            Name = name;
            Description = descripton;
            Category = category;
            Price = price;
        }
    }
}
