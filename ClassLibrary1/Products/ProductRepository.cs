﻿using System;
using System.Collections.Generic;

namespace OnlineStore.Products
{
    public static class ProductRepository
    {
        public static List<Product> AllProducts { get; set; }
        static ProductRepository()
        {
            AllProducts = new List<Product>();
        }

        public static List<Product> GetAllProducts()
        {
            if (AllProducts == null || AllProducts.Count == 0)
                throw new NullReferenceProductException("There is no products in database");
            return AllProducts;
        }
        public static Product GetProductByName(string name)
        {
            if (name == null)
                throw new ArgumentNullException(name, "Can not find the product by null name");
            Product result = default;
            foreach (Product productIteroator in AllProducts)
            {
                if (productIteroator.Name == name)
                {
                    result = productIteroator;
                }
            }
            if (result == null)
                throw new NotFindException("There is no product with such name");
            return result;
        }
    }
}
