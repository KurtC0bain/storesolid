﻿using System;
using OnlineStore.Products;
using System.Linq;

namespace OnlineStore.Users
{
    public class Administrator : Guest, IRegAndAdmin<Administrator>, IAdministrator
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public Order Order { get; set; }
        public Administrator(string name, string login, string password)
        {
            Name = name;
            Login = login;
            Password = password;
        }
        public void UpdateUserInfo(string previousName, string newName)
        {
            UserRepository.UpdateUserInfo(previousName, newName);
        }
        public RegisteredUser GetUserByName(string name)
        {
            return UserRepository.GetUserByName(name);
        }

        public void AddProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product", "Can not add empty item");
            if (ProductRepository.AllProducts.Contains(product))
                throw new AlreadyExistException("This item already exists in database");

            ProductRepository.AllProducts.Add(product);
        }
        public void UpdateProductInfo(Product product, string name)
        {
            if (product == null || string.IsNullOrEmpty(name))
                throw new ArgumentNullException("product", "Can not work with null info");

            for (int i = 0; i < ProductRepository.AllProducts.Count; i++)
            {
                if (ProductRepository.AllProducts[i].Name == name)
                    ProductRepository.AllProducts[i] = product;
            }
        }
        public void CreateOrder(string name)
        {
            Order = new Order(Name);
            Order.CreateOrder(name);
        }

        public void ChangeOrderStatus(int orderId, Order.OrderStatus orderStatus)
        {
            int OrderId = OrderRepository.OrderList.FirstOrDefault(x => x.Id == orderId).Id;
            OrderRepository.OrderList[OrderId - 1].Status = orderStatus;
        }

        public void ConfirmOrder()
        {
            OrderRepository.ConfirmOrder(Order);
        }
        public Guest LogOut(ref Administrator user)
        {
            user = null;
            return new Guest();
        }

    }
}
