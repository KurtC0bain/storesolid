﻿using System;
using System.Linq;

namespace OnlineStore.Users
{
    public static class Autentification
    {
        public static RegisteredUser Register(ref Guest guest, string name, string login, string password)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
                throw new ArgumentNullException(name, "Can not enter null information");
            UserRepository.RegisterUser(new RegisteredUser(name, login, password));
            guest = null;
            return new RegisteredUser(name, login, password);
        }
        public static RegisteredUser LogIn(ref Guest guest, string login, string password)
        {
            RegisteredUser registeredUser = UserRepository.Users.FirstOrDefault(x => x.Login == login && x.Password == password);
            if (registeredUser == null)
                throw new WrongLogInDataException("Incorrect login or password");
            return registeredUser;
        }
    }
}
