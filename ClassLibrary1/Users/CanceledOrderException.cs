﻿using System;
using System.Runtime.Serialization;

namespace OnlineStore.Users
{
    [Serializable]

    public class CanceledOrderException : Exception
    {
        public CanceledOrderException()
        {
        }

        public CanceledOrderException(string message)
            : base(message)
        {
        }

        public CanceledOrderException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected CanceledOrderException(SerializationInfo info, StreamingContext context)
: base(info, context)
        {

        }

    }
}
