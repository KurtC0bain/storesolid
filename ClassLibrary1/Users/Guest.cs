﻿using OnlineStore.Products;
using System.Collections.Generic;

namespace OnlineStore.Users
{
    public class Guest : IUser
    {
        public List<Product> GetAllGoods()
        {
            return ProductRepository.GetAllProducts();
        }
        public Product GetProductByName(string name)
        {
            return ProductRepository.GetProductByName(name);
        }

    }
}
