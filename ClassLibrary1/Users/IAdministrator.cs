﻿using OnlineStore.Products;

namespace OnlineStore.Users
{
    public interface IAdministrator
    {
        public void ChangeOrderStatus(int orderId, Order.OrderStatus orderStatus);
        public void UpdateProductInfo(Product product, string name);
        public void UpdateUserInfo(string previousName, string newName);
        public void AddProduct(Product product);
        public RegisteredUser GetUserByName(string name);
    }
}
