﻿using OnlineStore.Products;

namespace OnlineStore.Users
{
    public interface IRegAndAdmin<T>
    {
        public Guest LogOut(ref T user);
        public void CreateOrder(string name);
        public void ConfirmOrder();

    }
}
