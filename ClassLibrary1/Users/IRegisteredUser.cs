﻿namespace OnlineStore.Users
{
    public interface IRegisteredUser
    {
        public void UpdatePersonalInfo(string name, string login, string password);
        public void UpdatePersonalInfo(string name);
        public void UpdatePersonalInf(string login, string password);
        public void CancelOrder(int orderId);
        public void SetReceivedStatus(int orderId);
    }
}
