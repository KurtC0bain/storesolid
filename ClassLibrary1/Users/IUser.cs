﻿using OnlineStore.Products;
using System.Collections.Generic;

namespace OnlineStore.Users
{
    public interface IUser
    {
        public List<Product> GetAllGoods();
        public Product GetProductByName(string name);
    }
}
