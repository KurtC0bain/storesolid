﻿using System;
using System.Collections.Generic;
using System.Linq;
using OnlineStore.Products;

namespace OnlineStore.Users
{
    public class Order
    {
        public enum OrderStatus
        {
            New,
            CanceledByTheAdministrator,
            PaymentReceived,
            Sent,
            Received,
            Completed,
            CanceledByUser
        }
        public OrderStatus Status { get; set; }
        public int Id { get; set; } = 1;
        public string UserName { get; set; }

        public List<Product> ProductList { get; set; }
        public Order(string userName)
        {
            UserName = userName;
        }
        public void CreateOrder(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name", "Can not add empty product");
            if (ProductRepository.GetProductByName(name) == null)
                throw new NotFindException("Can not find the product to add");
            ProductList = new List<Product>();
            AddProduct(ProductRepository.GetProductByName(name));
            Status = OrderStatus.New;
        }
        public void AddProduct(Product product)
        {
            if (ProductList.FirstOrDefault(x => x == product) != null)
                throw new AlreadyExistException("This product already exists in the order");
            ProductList.Add(product);
        }
        public void RemoveProcdut(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(name, "Can not remove the product");
            foreach (var productIterator in ProductList)
            {
                if (productIterator.Name == name)
                    ProductList.Remove(productIterator);
            }
        }
    }
}
