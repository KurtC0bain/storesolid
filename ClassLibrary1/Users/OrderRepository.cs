﻿using System;
using System.Collections.Generic;

namespace OnlineStore.Users
{
    public static class OrderRepository
    {
        public static List<Order> OrderList { get; set; }
        public static int OrderCount { get; set; } = 1;
        static OrderRepository()
        {
            OrderList = new List<Order>();
        }
        public static void ConfirmOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order", "The order is empty");
            order.Id = OrderCount;
            OrderList.Add(order);
            OrderCount++;
        }

    }
}
