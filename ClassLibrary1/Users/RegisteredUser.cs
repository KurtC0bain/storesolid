﻿using OnlineStore.Products;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OnlineStore.Users
{
    public class RegisteredUser : Guest, IRegisteredUser, IRegAndAdmin<RegisteredUser>
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public Order Order { get; set; }

        public RegisteredUser(string name, string login, string password)
        {
            UpdatePersonalInfo(name, login, password);
        }
        public void UpdatePersonalInfo(string name, string login, string password)
        {
            Name = name;
            Login = login;
            Password = password;
        }
        public void UpdatePersonalInfo(string name)
        {
            Name = name;
        }
        public void UpdatePersonalInf(string login, string password)
        {
            Login = login;
            Password = password;
        }

        public void CreateOrder(string name)
        {
            Order = new Order(Name);
            Order.CreateOrder(name);
        }

        public void ConfirmOrder()
        {
            OrderRepository.ConfirmOrder(Order);
        }
        public void CancelOrder(int orderId)
        {
            OrderRepository.OrderList.First(x => x.Id == orderId).Status = Order.OrderStatus.CanceledByUser;
        }
        public void SetReceivedStatus(int orderId)
        {
            Order.OrderStatus orderStatus = OrderRepository.OrderList.First(x => x.Id == orderId).Status;
            if (orderStatus == Order.OrderStatus.CanceledByTheAdministrator || orderStatus == Order.OrderStatus.CanceledByUser)
                throw new CanceledOrderException("Can not be recieved. It is canceled");
            OrderRepository.OrderList.First(x => x.Id == orderId).Status = Order.OrderStatus.Received;
        }
        public List<Order> GetOrderHistory()
        {
            return (List<Order>)OrderRepository.OrderList.Where(x => x.UserName == Name);
        }
        public Guest LogOut(ref RegisteredUser user)
        {
            user = null;
            return new Guest();
        }

    }
}
