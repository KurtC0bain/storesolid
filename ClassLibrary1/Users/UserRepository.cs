﻿using System;
using System.Collections.Generic;
using System.Linq;
using OnlineStore;

namespace OnlineStore.Users
{
    public static class UserRepository
    {
        public static List<RegisteredUser> Users { get; set; }
        static UserRepository()
        {
            Users = new List<RegisteredUser>();
        }
        public static void RegisterUser(RegisteredUser registeredUser)
        {
            if (registeredUser == null)
                throw new ArgumentNullException(nameof(registeredUser), "User is null or empty");
            if (Users.FirstOrDefault(x => x.Login == registeredUser.Login && x.Password == registeredUser.Password) != null)
                throw new AlreadyExistException("That user is already registered");
            Users.Add(registeredUser);
        }
        public static RegisteredUser GetUserByName(string name)
        {
            RegisteredUser registeredUser = Users.FirstOrDefault(x => x.Name == name);
            if (registeredUser == null)
                throw new NotFindException("Can not find such user");
            return registeredUser;
        }
        public static void UpdateUserInfo(string previousName, string newName)
        {
            if (string.IsNullOrEmpty(previousName) || string.IsNullOrEmpty(newName))
                throw new ArgumentNullException(previousName, "Can not work with null info");

            RegisteredUser registerUser = GetUserByName(previousName);
            Users.Remove(GetUserByName(previousName));
            registerUser.Name = newName;
            Users.Add(registerUser);
        }
    }
}
