﻿using System;
using System.Runtime.Serialization;

namespace OnlineStore.Users
{
    [Serializable]

    public class WrongLogInDataException : Exception
    {
        public WrongLogInDataException()
        {
        }

        public WrongLogInDataException(string message)
            : base(message)
        {
        }

        public WrongLogInDataException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected WrongLogInDataException(SerializationInfo info, StreamingContext context)
: base(info, context)
        {

        }

    }
}
