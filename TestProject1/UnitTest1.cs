using NUnit.Framework;
using OnlineStore.Products;
using OnlineStore.Users;
using OnlineStore;
using System.Linq;
using System.Collections.Generic;

namespace OnlineStoreNUnitTests
{
    public class OnlineStoreNUnitTests
    {
        [Test]
        public void GetProduct_Guest_Recieve_All_Products_Returns_All_Product_List()
        {
            Administrator administrator = new Administrator("admin", "adm", "123");
            administrator.AddProduct(new Product("Lenovo Legion 5", "Gaming laptop", "Laptops", 27999));
            administrator.AddProduct(new Product("Lenovo IdeaPad L340", "Gaming laptop", "Laptops", 22999));

            Guest guest = new Guest();
            List<Product> products = guest.GetAllGoods();

            Assert.AreEqual(ProductRepository.AllProducts, products);
        }
        [Test]
        public void GetProduct_Guest_Doesnt_Recieve_All_Products_Throws()
        {
            Guest guest = new Guest();

            Assert.Throws<NullReferenceProductException>(() => guest.GetAllGoods());
        }

        [Test]
        public void GetProductByName_ProductFromProductRepository_ReturnsProductLenovo()
        {
            Administrator administrator = new Administrator("admin", "adm", "123");
            administrator.AddProduct(new Product("Lenovo Legion 5", "Gaming laptop", "Laptops", 27999));
            administrator.AddProduct(new Product("Lenovo IdeaPad L340", "Gaming laptop", "Laptops", 22999));

            Guest guest = new Guest();
            string name = "Lenovo Legion 5";

            Product product = guest.GetProductByName(name);

            Assert.AreEqual(ProductRepository.GetProductByName(name), product);

        }
        [Test]
        public void GetProductByName_ProductFromProductRepository_ReturnsException()
        {
            Administrator administrator = new Administrator("admin", "adm", "123");
            administrator.AddProduct(new Product("Lenovo Legion 5", "Gaming laptop", "Laptops", 27999));
            administrator.AddProduct(new Product("Lenovo IdeaPad L340", "Gaming laptop", "Laptops", 22999));

            Guest guest = new Guest();
            string name = "Lenovo";

            Assert.Throws<NotFindException>(() => guest.GetProductByName(name));


        }
        [Test]
        public void Register_GuestRegistation_ReturnsRegisteredUser()
        {
            Guest guest = new Guest();

            RegisteredUser registeredUser = Autentification.Register(ref guest, "Max", "max123", "qwerty");



            Assert.IsNotNull(registeredUser);
            Assert.IsTrue(registeredUser is RegisteredUser);
        }
        [Test]
        public void Register_AddUserToRepo_AlreadyExistException()
        {
            Guest guest = new Guest();
            Autentification.Register(ref guest, "Max", "max123", "qwerty");
            Guest guest1 = new Guest();
            Assert.Throws<AlreadyExistException>(() => Autentification.Register(ref guest1, "Max", "max123", "qwerty"));
        }
        [Test]
        public void LogIn_EnterUserAccount_ReturnsRegisteredUser()
        {
            Guest guest = new Guest();
            UserRepository.RegisterUser(new RegisteredUser("max", "max", "qwerty"));

            RegisteredUser registeredUser = Autentification.LogIn(ref guest, "max", "qwerty");
            Assert.IsNotNull(registeredUser);
            Assert.IsTrue(registeredUser is RegisteredUser);
        }
        [Test]
        public void LogIn_TryEnterWithIncorrectLoginOrPassword_WrongLogInDataException()
        {
            Guest guest = new Guest();
            UserRepository.RegisterUser(new RegisteredUser("max", "max", "qwerty"));
            UserRepository.RegisterUser(new RegisteredUser("max", "maxx", "qwerty"));

            Assert.Throws<WrongLogInDataException>(() => Autentification.LogIn(ref guest, "maxx", "qwertyy"));
        }
        [Test]
        public void LogOut_RegisteredUserLeaveTheAccount_ReturnsNewGuest()
        {
            Guest guest = new Guest();
            UserRepository.RegisterUser(new RegisteredUser("max", "max", "qwerty"));

            RegisteredUser registeredUser = Autentification.LogIn(ref guest, "max", "qwerty");
            Administrator administrator = new Administrator("admin", "adm", "123");

            Guest loggedOut = registeredUser.LogOut(ref registeredUser);

            Assert.IsNotNull(loggedOut);
            Assert.IsNull(registeredUser);

        }
        [Test]
        public void UpdateUserInfo_RenameUsere()
        {
            Administrator administrator = new Administrator("admin", "adm", "123");
            UserRepository.RegisterUser(new RegisteredUser("max", "max", "qwerty"));

            administrator.UpdateUserInfo("max", "vlad");

            Assert.AreEqual(UserRepository.Users[0].Name, "vlad");
        }
        [Test]
        public void UpdateUserInfo_RenameUsere_NotFindException()
        {
            Administrator administrator = new Administrator("admin", "adm", "123");
            UserRepository.RegisterUser(new RegisteredUser("max", "max", "qwerty"));

            Assert.Throws<NotFindException>(() => administrator.UpdateUserInfo("maxx", "vlad"));
        }
        public void CreateOrder_RegUserCreateOrder_IfExistInProductList()
        {
            Administrator administrator = new Administrator("admin", "adm", "123");
            administrator.AddProduct(new Product("Lenovo Legion 5", "Gaming laptop", "Laptops", 27999));
            administrator.AddProduct(new Product("Lenovo IdeaPad L340", "Gaming laptop", "Laptops", 22999));


            Guest guest = new Guest();
            RegisteredUser registeredUser = Autentification.Register(ref guest, "Max", "max123", "qwerty");

            registeredUser.CreateOrder("Lenovo Legion 5");
            Assert.IsNotNull(registeredUser.Order.ProductList[0]);
        }

        [Test]
        public void CreateOreder_ChechOrderStatus_IsNew()
        {
            Administrator administrator = new Administrator("admin", "adm", "123");
            administrator.AddProduct(new Product("Lenovo Legion 5", "Gaming laptop", "Laptops", 27999));
            administrator.AddProduct(new Product("Lenovo IdeaPad L340", "Gaming laptop", "Laptops", 22999));

            Guest guest = new Guest();
            RegisteredUser registeredUser = Autentification.Register(ref guest, "Max", "max123", "qwerty");

            registeredUser.CreateOrder("Lenovo Legion 5");
            Assert.IsTrue(registeredUser.Order.Status == Order.OrderStatus.New);
        }
        [Test]
        public void ConfirmOrder_RegUserConfirmOrder_IsConfirmed()
        {
            Administrator administrator = new Administrator("admin", "adm", "123");
            administrator.AddProduct(new Product("Lenovo Legion 5", "Gaming laptop", "Laptops", 27999));
            administrator.AddProduct(new Product("Lenovo IdeaPad L340", "Gaming laptop", "Laptops", 22999));

            Guest guest = new Guest();
            RegisteredUser registeredUser = Autentification.Register(ref guest, "Max", "max123", "qwerty");
            registeredUser.CreateOrder("Lenovo Legion 5");
            registeredUser.ConfirmOrder();

            Assert.IsNotNull(OrderRepository.OrderList[0]);
        }
        [Test]
        public void ChangeOrderStatus_OrderStatusChangeByAdmin_CanceledByAdministrator()
        {
            Administrator administrator = new Administrator("admin", "adm", "123");
            administrator.AddProduct(new Product("Lenovo Legion 5", "Gaming laptop", "Laptops", 27999));
            administrator.AddProduct(new Product("Lenovo IdeaPad L340", "Gaming laptop", "Laptops", 22999));

            Guest guest = new Guest();
            RegisteredUser registeredUser = Autentification.Register(ref guest, "Max", "max123", "qwerty");
            registeredUser.CreateOrder("Lenovo Legion 5");
            registeredUser.ConfirmOrder();

            administrator.ChangeOrderStatus(1, Order.OrderStatus.CanceledByTheAdministrator);

            Assert.AreEqual(Order.OrderStatus.CanceledByTheAdministrator, OrderRepository.OrderList[0].Status);
        }
        [Test]
        public void SetReceivedStatus_TryToSetRecievedStatus_CanceledOrderException()
        {
            Administrator administrator = new Administrator("admin", "adm", "123");
            administrator.AddProduct(new Product("Lenovo Legion 5", "Gaming laptop", "Laptops", 27999));
            administrator.AddProduct(new Product("Lenovo IdeaPad L340", "Gaming laptop", "Laptops", 22999));

            Guest guest = new Guest();
            RegisteredUser registeredUser = Autentification.Register(ref guest, "Max", "max123", "qwerty");
            registeredUser.CreateOrder("Lenovo Legion 5");
            registeredUser.ConfirmOrder();

            administrator.ChangeOrderStatus(1, Order.OrderStatus.CanceledByTheAdministrator);

            Assert.Throws<CanceledOrderException>(() => registeredUser.SetReceivedStatus(OrderRepository.OrderList.First(x => x.UserName == registeredUser.Name).Id));
        }
    }
}